/*
 * Copyright (C) 2017 Teodor MAMOLEA <Teodor.Mamolea@gmail.com>
 *
 * ******************************************************************************
 *
 * DOWHATYOUWANTTODO
 *
 * ******************************************************************************
 */
package com.me0x.pasteit;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JTextField;

class FileChooser extends JPanel implements ActionListener {

    private String path = System.getProperty("user.home") + System.getProperty("file.separator") + "pasteit.txt";
    private JFileChooser fileChooser = new JFileChooser();
    private JButton button = new JButton("Save to...");
    private JTextField textField = new JTextField(path, 20);

    FileChooser() {
        super();

        setLayout(new BorderLayout());
        button.addActionListener(this);
        textField.setEditable(false);
        textField.setBackground(Color.WHITE);

        add(textField);
        add(button, BorderLayout.EAST);
    }

    String getPath() {
        return path;
    }

    @Override
    public void actionPerformed(final ActionEvent event) {
        if (JFileChooser.APPROVE_OPTION == fileChooser.showOpenDialog(FileChooser.this)) {
            path = fileChooser.getSelectedFile().getAbsolutePath();
            textField.setText(path);
        }
    }

    @Override
    public void setEnabled(final boolean b) {
        button.setEnabled(b);
    }
}
