/*
 * Copyright (C) 2017 Teodor MAMOLEA <Teodor.Mamolea@gmail.com>
 *
 * ******************************************************************************
 *
 * DOWHATYOUWANTTODO
 *
 * ******************************************************************************
 */
package com.me0x.pasteit;

import java.awt.EventQueue;

public class Main {
    public static void main(final String... args) {
        EventQueue.invokeLater(PasteitFrame::new);
    }
}
