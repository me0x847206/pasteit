/*
 * Copyright (C) 2017 Teodor MAMOLEA <Teodor.Mamolea@gmail.com>
 *
 * ******************************************************************************
 *
 * DOWHATYOUWANTTODO
 *
 * ******************************************************************************
 */
package com.me0x.pasteit;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static java.awt.Color.GREEN;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

class PasteitFrame extends JFrame implements ActionListener {

    private PasteitClipboard clipboard = new PasteitClipboard();
    private JButton button = new JButton("START");
    private FileChooser fileChooser = new FileChooser();
    private JCheckBox append = new JCheckBox("append to file", true);
    private JCheckBox separator = new JCheckBox("separe with new line", true);

    private boolean isActive;

    PasteitFrame() {
        super("Pasteit by me0x847206");

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);

        button.setBackground(GREEN);
        button.addActionListener(this);

        JPanel north = new JPanel();
        JPanel center = new JPanel();
        //JPanel south = new JPanel();

        north.setLayout(new BorderLayout());
        center.setLayout(new GridLayout(3, 1));

        north.add(fileChooser);
        center.add(append);
        center.add(separator);
        center.add(button);

        add(north, BorderLayout.NORTH);
        add(center);

        setSize(400, 150);
        setResizable(false);
        setVisible(true);
    }

    private void init() {

    }

    @Override
    public void actionPerformed(ActionEvent event) {
        fileChooser.setEnabled(isActive);
        separator.setEnabled(isActive);
        append.setEnabled(isActive);

        if (isActive) {
            button.setText("Start");
            button.setBackground(GREEN);
            clipboard.stop();
            setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        } else {
            button.setText("Stop");
            button.setBackground(Color.RED);
            clipboard.setPath(fileChooser.getPath());
            clipboard.setAppend(append.isSelected());
            clipboard.setSeparator(separator.isSelected() ? System.getProperty("line.separator") : "");
            clipboard.start();
            setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        }

        isActive = !isActive;
    }
}
