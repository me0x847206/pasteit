/*
 * Copyright (C) 2017 Teodor MAMOLEA <Teodor.Mamolea@gmail.com>
 *
 * ******************************************************************************
 *
 * DOWHATYOUWANTTODO
 *
 * ******************************************************************************
 */
package com.me0x.pasteit;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.io.FileWriter;

class PasteitClipboard implements ClipboardOwner {

    private Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
    private String path, separator = System.getProperty("line.separator");
    private boolean append = false;
    private boolean isStarted = false;
    private FileWriter out;

    void start() {
        isStarted = true;

        clipboard.setContents(new StringSelection(""), this);

        try {
            out = new FileWriter(path, append);
        } catch (final Exception e) {
            out = null;
            System.out.println("Clipboard.init exception: " + e);
        }
    }

    @Override
    public void lostOwnership(final Clipboard clipboard, final Transferable contents) {
        if (!isStarted) {
            return;
        }

        String string = "";
        try {
            out.write((string = (String) clipboard.getContents(this).getTransferData(DataFlavor.stringFlavor)) + separator);
        } catch (Exception e) {
            System.out.println("Clipboard.lostOwnership exception: " + e);
        }

        clipboard.setContents(new StringSelection(string), this);
    }

    void setPath(final String path) {
        this.path = path;
    }

    void setAppend(final boolean b) {
        append = b;
    }

    void setSeparator(final String separator) {
        this.separator = separator;
    }

    void stop() {
        isStarted = false;
        clipboard.setContents(new StringSelection(""), null);

        try {
            out.close();
        } catch (final Exception e) {

        }
    }
}
